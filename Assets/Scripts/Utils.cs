﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class Utils {
    public static Vector3 Centroid(this IList<Vector3> points) {
        return points.Aggregate((a, b) => a + b) / points.Count;
    }

    public static Vector3 WeightedCentroid(this IList<Vector4> points) {
        var tmp = Vector3.zero;
        var mass = 0f;

        foreach (var point in points) {
            tmp += point.XYZ() * point.w;
            mass += point.w;
        }

        tmp /= mass;

        return tmp;
    }

    public static Vector3 SlerpByWeight(this IList<Vector4> directions) {
        var avg = directions[0].XYZ();
        var mass = directions[0].w;
        for (var i = 1; i < directions.Count; i++) {
            var vec = directions[i];
            mass += vec.w;
            var propWeight = vec.w / mass;
            avg = Vector3.Slerp(avg, vec.XYZ(), propWeight);
        }
        avg.z = 0;

        return avg.normalized;
    }

    public static Vector3 XYZ(this Vector4 vec) {
        return new Vector3(vec.x, vec.y, vec.z);
    }

    public static Vector4 Weighted(this Vector3 vec, float weight) {
        return new Vector4(vec.x, vec.y, vec.z, weight);
    }
} 
