﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu]
public class SeparationBehaviour : Behaviour {
    public float avoidRadius = 1;

    public override Vector3 Compute(Agent current, IEnumerable<Agent> otherAgents) {
        var currentPos = current.transform.position;

        var offsets = otherAgents.Select(agent => agent.transform.position)
            .Select(pos => currentPos - pos)
            .Where(offset => offset.magnitude < avoidRadius)
            .ToList();

        if (offsets.Count == 0) return Vector3.zero;

        return offsets.Select(offset => (offset / offset.sqrMagnitude).Weighted(1 / offset.sqrMagnitude))
            .ToList()
            .SlerpByWeight();
    }
}
