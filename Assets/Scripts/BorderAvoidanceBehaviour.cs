﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BorderAvoidanceBehaviour : Behaviour {

    public float left, right, top, bottom;
    
    public override Vector3 Compute(Agent current, IEnumerable<Agent> otherAgents) {
        var pos = current.transform.position;
        var target = Vector3.zero;

        if (pos.x < left) target.x = 1;
        else if (pos.x > right) target.x = -1;

        if (pos.y < bottom) target.y = 1;
        else if (pos.y > top) target.y = -1;

        return target.normalized;

    }
}