﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu]
public class CohesionBehaviour : Behaviour {
    public override Vector3 Compute(Agent current, IEnumerable<Agent> otherAgents) {
        var others = otherAgents.ToList();
        if (others.Count == 0) return Vector3.zero;

        var target = others.Select(agent => agent.transform.position).ToList().Centroid();
        return (target - current.transform.position).normalized;
    }
}